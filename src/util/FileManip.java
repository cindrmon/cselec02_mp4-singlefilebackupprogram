package util;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileManip {
	public static void fileDuper(String fileName) {
		
		// set backup file name to end with "_bak"
		int extensionDotIdex = fileName.lastIndexOf('.');
		String fileBakName = fileName.substring(0, extensionDotIdex) + "_bak" + fileName.substring(extensionDotIdex);
		
		File fileBak = new File(fileBakName);
		
		// File IO Stream
		FileInputStream fileIStream = null;
		FileOutputStream fileOStream = null;
		
		// dataByte pointer
		int dataByte;
		
		try {
			// read from original file
			fileIStream = new FileInputStream(fileName);
			// getting read databyte and writing it on file with "_bak"
			fileOStream = new FileOutputStream(fileBakName);
			
			// stream write marker
			boolean isDone = false;
			
			
			do {
				// read from original file
				dataByte = fileIStream.read();
				// EOL Indicator is -1
				if(dataByte == -1) {
					fileOStream.write(dataByte);
					isDone = true;
				}
				// write to backup file
				else {
					fileOStream.write(dataByte);
				}
			} while(!isDone);
			
			if(isDone) {
				System.out.println("Successfully created backup file!");
				System.out.println(fileBak.getName() + " on " + fileBak.getParent());
				System.out.println("\nExecuting file to check if corrupted...");
				Desktop.getDesktop().open(fileBak);
			}
			
		} catch (FileNotFoundException fnfExc) {
			System.err.println("File Not Found... Please Try again");
		} catch (IOException ioExc) {
			System.err.println(ioExc.getMessage());
		} catch (Exception exc) {
			System.err.println("Something wrong happened... " + exc.getMessage());
		}
	}
}
